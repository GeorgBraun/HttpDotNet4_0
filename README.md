# Project HttpDotNet4_0

Example Project(s) how to do HTTP-Requests (GET, POST, ...) in .NET 4.0.

There are several options available:
* HttpWebRequest: More options, but more work. Uses delegates for async operation.
* WebClient: Simpler, less flexible. Uses event-handlers/callbacks on the same thread for async operation. Much simpler!
* HttpClient: Quite convenient, but requires .NET 4.5 or higher.

Helpful links:
* http://codecaster.nl/blog/2015/11/webclient-httpwebrequest-httpclient-perform-web-requests-net/
* https://www.c-sharpcorner.com/uploadfile/dhananjaycoder/webclient-and-httpwebrequest-class/
* http://www.codingvision.net/networking/c-sending-data-using-get-or-post
* https://www.hanselman.com/blog/HTTPPOSTsAndHTTPGETsWithWebClientAndCAndFakingAPostBack.aspx

