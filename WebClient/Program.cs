﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

// Wichtiger Unterschied:
// WebClient is a higher-level abstraction built on top of HttpWebRequest

// Schöner Vergleich zwischen HttpWebRequest, WebClient und HttpClient (.net 4.5+):
// http://codecaster.nl/blog/2015/11/webclient-httpwebrequest-httpclient-perform-web-requests-net/

// Schöner Vergleich zwischen HttpWebRequest und WebClient
// https://www.c-sharpcorner.com/uploadfile/dhananjaycoder/webclient-and-httpwebrequest-class/
// Daraus: Ein sehr wichtiges Argument für den WebClient:
// Callback for WebClient is raised for HTTP Response and it is invoked on the same user interface thread. 
// So it is easier to update property of UI element.
// That is why it is easy with WebClient to bind UI properties with data of Http response. 

// Auch interessant wegen GET und POST:
// http://www.codingvision.net/networking/c-sending-data-using-get-or-post
// https://www.hanselman.com/blog/HTTPPOSTsAndHTTPGETsWithWebClientAndCAndFakingAPostBack.aspx

namespace WebClientExample {
  class Program {
    static void Main(string[] args) {
      Console.WriteLine("Hallo WebClient-Class");

      string Url = "http://fk06.hm.edu:60606/api/users"; // Beispiel RestAPI, die ein JSON-Array liefert

      WebClient webClient = new WebClient();

      // Synchroner Ansatz:
      // Quelle: https://msdn.microsoft.com/de-de/library/fhd1f0sw(v=vs.110).aspx#Anchor_3
      string Response =  webClient.DownloadString(Url);
      Console.WriteLine(Response);

      // Asynchroner Ansatz, besser für GUI-Programme z.B. mit WinForms oder WPF:
      // Quelle: https://www.c-sharpcorner.com/uploadfile/dhananjaycoder/webclient-and-httpwebrequest-class/
      webClient.DownloadStringCompleted += HandleHttpResponse;
      webClient.DownloadStringAsync(new Uri(Url));

      Console.WriteLine("Zum Beenden Taste drücken ...");
      Console.ReadKey();
    }

    private static void HandleHttpResponse(object sender, DownloadStringCompletedEventArgs e) {
      string Result = e.Result;
      Console.WriteLine("=======================================");
      Console.WriteLine(Result);
      Console.WriteLine("=======================================");
    }
  }
}
